package com.nesterov;

import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.Base64;
import java.util.Scanner;

import javax.crypto.Cipher;

public class Main {

    public static void main(String[] args) throws Exception {
        Scanner scanner = new Scanner(System.in);
        // Создание ключевой пары.
        KeyPair keyPair = generateKeyPair();

        // Получение открытого и закрытого ключа.
        PublicKey publicKey = keyPair.getPublic();
        PrivateKey privateKey = keyPair.getPrivate();

        System.out.println("Введите сообщение:");
        // Шифрование сообщения.
        String message = scanner.next();
        byte[] encryptedMessage = encrypt(message, publicKey);

        // Вывод зашифрованного сообщения в виде строки.
        String encryptedMessageString = Base64.getEncoder().encodeToString(encryptedMessage);
        System.out.println("Зашифрованное сообщение: " + encryptedMessageString);

        // Расшифровка сообщения.
        byte[] decryptedMessage = decrypt(encryptedMessage, privateKey);

        // Вывод расшифрованного сообщения в виде строки.
        String decryptedMessageString = new String(decryptedMessage);
        System.out.println("Расшифрованное сообщение: " + decryptedMessageString);
    }

    /**
     * Метод для генерации ключевой пары по алгоритму RSA
     * @return сгенерированная ключевая пара
     * @throws NoSuchAlgorithmException
     */
    public static KeyPair generateKeyPair() throws NoSuchAlgorithmException {
        KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("RSA");
        keyPairGenerator.initialize(2048);
        return keyPairGenerator.genKeyPair();
    }

    /**
     * Метод для шифрования сообщения по алгоритму RSA
     * @param message сообщение для шифрования
     * @param publicKey открытый ключ
     * @return зашифрованное сообщение в виде массива байтов
     * @throws Exception
     */
    public static byte[] encrypt(String message, PublicKey publicKey) throws Exception {
        Cipher cipher = Cipher.getInstance("RSA");
        cipher.init(Cipher.ENCRYPT_MODE, publicKey);
        return cipher.doFinal(message.getBytes());
    }

    /**
     * Метод для расшифровки сообщения по алгоритму RSA
     * @param encryptedMessage зашифрованное сообщение в виде массива байтов
     * @param privateKey закрытый ключ
     * @return расшифрованное сообщение в виде массива байтов
     * @throws Exception
     */
    public static byte[] decrypt(byte[] encryptedMessage, PrivateKey privateKey) throws Exception {
        Cipher cipher = Cipher.getInstance("RSA");
        cipher.init(Cipher.DECRYPT_MODE, privateKey);
        return cipher.doFinal(encryptedMessage);
    }
}